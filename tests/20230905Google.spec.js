// @ts-check

import { test, expect } from '@playwright/test';

test('Looking for a Substring', async ({ page }) => {
  await page.goto('https://www.google.com/');

// Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/.*Google/);
});

test('Checking for a button', async ({ page }) => {
  await page.goto('https://www.google.com');
  
  // Expect a button for Feeling
  await expect(page.getByRole('button', { name: 'Feeling' })).toBeVisible();
});

test('Validating Search Results', async ({ page }) => {
  await page.goto('https://www.google.com/');

  await page.getByLabel('Search', { exact: true }).click();
  await page.getByLabel('Search', { exact: true }).fill('playwright vs selenium for testing');

 // Expect at least one link that contains the word "playwright"
  await page.getByRole('link', { name: /Playwright/ })



});