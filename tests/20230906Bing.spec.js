// @ts-check

import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://www.bing.com/');
  await page.getByPlaceholder('Search the web').click();
  await page.getByPlaceholder('Search the web').fill('playwright vs cypress for testing');
  await page.getByRole('link', { name: 'Playwright vs Cypress: A Comparison' }).click({
    modifiers: ['Control']
  });
});