// @ts-check
const { test, expect } = require('@playwright/test');

test('test', async ({ page }) => {
  await page.goto('https://www.cnn.com/');

// Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/.*News/);
});

test('Americas', async ({ page }) => {
  await page.goto('https://www.cnn.com/world');
  
  // Expect a heading for Americas
  await expect(page.getByRole('heading', { name: 'Americas' })).toBeVisible();

 // await page.goto('https://www.cnn.com/world');
  await page.getByRole('link', { name: 'Weather that drove eastern Canada’s devastating wildfires made twice as likely by climate change' }).click();
});