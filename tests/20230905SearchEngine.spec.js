import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {

  await page.goto('https://www.bing.com/');
  //await page.waitForNavigation();

  await page.getByRole('button', { name: 'Accept' }).click();
  await page.getByPlaceholder('Search').click();
  await page.getByPlaceholder('Search').click();

  //await page.getByPlaceholder('Search').fill('playwright vs cypress for testing');
  //await page.getByRole('link', { name: 'Playwright vs Selenium vs Cypress: A Detailed Comparison', exact: true }).click({
  //  modifiers: ['Control']
  //});

  //  const searchText = await page.inputValue('[placeholder="Search"]');

    const searchText = await page.evaluate(() => prompt('Please enter your search text:'));
  
   // await page.fill('[placeholder="Search"]', searchText);
    await page.getByRole('link', { name: searchText, exact: true }).click({
    modifiers: ['Control']
    
  });
});