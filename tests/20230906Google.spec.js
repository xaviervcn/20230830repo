// @ts-check

import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://www.google.com/');
  await page.getByLabel('Search', { exact: true }).click();
  await page.getByLabel('Search', { exact: true }).fill('playwright vs cypress for testing');
  await page.getByRole('link', { name: 'Playwright vs Cypress: Why QA Wolf chose modern tools over ... QA Wolf https://www.qawolf.com › blog › why-qa-wolf-chose-pla...' }).click();
  await page.goto('https://www.google.com/search?q=playwright+vs+cypress+for+testing&sca_esv=563146365&source=hp&ei=Usb4ZM7OLeXT9AOp-q24Dw&iflsig=AD69kcEAAAAAZPjUYiw6UMA7kEbOIAT784WtRvfd7Ud1&ved=0ahUKEwjOv7CB0JaBAxXlKX0KHSl9C_cQ4dUDCAs&uact=5&oq=playwright+vs+cypress+for+testing&gs_lp=Egdnd3Mtd2l6IiFwbGF5d3JpZ2h0IHZzIGN5cHJlc3MgZm9yIHRlc3RpbmcyBRAhGKABMggQIRgWGB4YHTIIECEYFhgeGB1Igl5Q0gRY6VNwB3gAkAEAmAFvoAHgE6oBBDM3LjK4AQPIAQD4AQGoAgrCAhAQABgDGI8BGOUCGOoCGIwDwgIQEC4YAxiPARjlAhjqAhiMA8ICCxAAGIAEGLEDGIMBwgIOEC4YgAQYsQMYxwEY0QPCAhEQLhiABBixAxiDARjHARjRA8ICDhAuGIoFGLEDGMcBGNEDwgIREC4YigUYsQMYgwEYxwEY0QPCAgUQABiABMICDhAuGIAEGLEDGMcBGK8BwgILEAAYigUYsQMYgwHCAggQABiABBjJA8ICCBAAGIoFGJIDwgIIEAAYgAQYsQPCAg4QLhiABBixAxiDARjlBMICCxAuGIAEGLEDGIMBwgILEC4YgAQYxwEYrwHCAgYQABgWGB7CAggQABiKBRiGAw&sclient=gws-wiz');
});