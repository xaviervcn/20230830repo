import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://www.google.com/');
  await page.getByLabel('Search', { exact: true }).click();
  await page.getByLabel('Search', { exact: true }).fill('playwright vs selenium for testing');
  await page.getByRole('link', { name: 'Playwright vs Selenium: What are the Main Differences and ... Applitools https://applitools.com › Blog › Getting Started' }).click();
});