// @ts-check

import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://www.cbc.ca/');
  await page.getByRole('link', { name: 'Local updates' }).click();
  await page.getByRole('button', { name: 'Change' }).click();
  await page.getByLabel('British Columbia').check();
  await page.getByRole('button', { name: 'Update Preference' }).click();
  const page1Promise = page.waitForEvent('popup');
  await page.getByRole('link', { name: 'Corporate Info' }).click();
  const page1 = await page1Promise;
  await page1.goto('https://cbc.radio-canada.ca/en');
});