import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://www.cbc.ca/');
  await page.getByRole('link', { name: 'Local updates' }).click();
  await page.getByText('Currently Selected: Local').click();
  await page.getByRole('link', { name: 'Top Stories' }).click();
  await page.getByRole('link', { name: 'Local', exact: true }).click();
});